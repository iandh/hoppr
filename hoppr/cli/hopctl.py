"""
Framework for manipulating bundles for airgapped transfers.
"""
import ctypes
import sys

from pathlib import Path
from platform import python_version
from typing import List

from typer import Argument, Option, Typer, echo, secho, style

from hoppr import __version__
from hoppr import main as hopctl

# Windows flags and types
NT_ENABLE_ECHO_INPUT = 0x0004
NT_ENABLE_LINE_INPUT = 0x0002
NT_ENABLE_PROCESSED_INPUT = 0x0001
NT_CONSOLE_FLAGS = NT_ENABLE_ECHO_INPUT | NT_ENABLE_LINE_INPUT | NT_ENABLE_PROCESSED_INPUT
NT_STD_OUTPUT_HANDLE = ctypes.c_uint(-11)

# Enable ANSI processing on Windows systems
if sys.platform == "win32":  # pragma: no cover
    nt_kernel = ctypes.WinDLL(name="kernel32.dll")

    nt_kernel.SetConsoleMode(nt_kernel.GetStdHandle(NT_STD_OUTPUT_HANDLE), NT_CONSOLE_FLAGS)

CREDENTIALS_FILE_OPTION: Path = Option(
    None,
    "-c",
    "--credentials",
    help="Specify credentials config for services",
    envvar="HOPPR_CREDS_CONFIG",
)

LOG_FILE_OPTION: Path = Option(
    None,
    "-l",
    "--log",
    help="File to which log will be written",
    envvar="HOPPR_LOG_FILE",
)

MANIFEST_FILE_ARGUMENT: Path = Argument(
    "manifest.yml",
    help="Path to manifest file",
    expose_value=True,
)

STRICT_OPTION: bool = Option(
    True,
    "--strict/--no-strict",
    help="Utilize only manifest repositories for package collection",
    envvar="HOPPR_STRICT_REPOS",
)

TRANSFER_FILE_OPTION: Path = Option(
    "transfer.yml",
    "-t",
    "--transfer",
    help="Specify transfer config",
    envvar="HOPPR_TRANSFER_CONFIG",
)

VERBOSE_OPTION: bool = Option(
    False,
    "-v",
    "--debug",
    "--verbose",
    help="Enable debug output",
)

app = Typer(context_settings=dict(help_option_names=["-h", "--help"]))


@app.callback()
def callback():
    """
    Define, validate, and transfer dependencies between environments.
    """


@app.command()
def bundle(
    # pylint: disable=unused-argument
    # pylint: disable=too-many-arguments
    manifest_file: Path = MANIFEST_FILE_ARGUMENT,
    credentials_file: Path = CREDENTIALS_FILE_OPTION,
    transfer_file: Path = TRANSFER_FILE_OPTION,
    log_file: Path = LOG_FILE_OPTION,
    verbose: bool = VERBOSE_OPTION,
    strict_repos: bool = STRICT_OPTION,
):
    """
    Run the stages specified in the transfer config
    file on the content specified in the manifest
    """
    hopctl.bundle(**locals())  # pragma: no cover


@app.command()
def validate(
    # pylint: disable=unused-argument
    input_files: List[Path],
    credentials_file: Path = CREDENTIALS_FILE_OPTION,
    transfer_file: Path = TRANSFER_FILE_OPTION,
):
    """
    Validate multiple manifest files for schema errors.
    """
    hopctl.validate(**locals())  # pragma: no cover


@app.command()
def version():
    """
    Print version information for `hoppr`
    """
    hippo = Path(__file__).parent / ".." / "resources" / "hoppr-hippo.ansi"
    with hippo.open(mode="rb") as ansi:
        echo(ansi.read())

    secho(f"{style(text='Hoppr Framework Version', fg='green')} : {__version__}")
    secho(f"{style(text='Python Version         ', fg='green')} : {python_version()}")
