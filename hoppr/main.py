"""
Framework for manipulating bundles for airgapped transfers.
"""
import logging
import sys

from pathlib import Path
from typing import List

from hoppr import __version__
from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.configs.transfer import Transfer
from hoppr.processor import HopprProcessor


def bundle(  # pylint: disable=too-many-arguments
    manifest_file: Path,
    credentials_file: Path,
    transfer_file: Path,
    log_file: Path,
    verbose: bool = False,
    strict_repos: bool = True,
):
    """
    Run the stages specified in the transfer config
    file on the content specified in the manifest
    """

    metadata_files = [manifest_file, transfer_file]

    if credentials_file is not None:
        Credentials.load_file(file=credentials_file)
        metadata_files.append(credentials_file)

    manifest = Manifest.load_file(file=manifest_file)
    transfer = Transfer.load_file(file=transfer_file)
    log_level = logging.DEBUG if verbose else logging.INFO

    processor = HopprProcessor(transfer=transfer, manifest=manifest, log_level=log_level)
    processor.metadata_files = metadata_files

    result = processor.run(log_file=log_file, strict_repos=strict_repos)

    if result.is_fail():
        sys.exit(1)


def validate(
    input_files: List[Path],
    credentials_file: Path,
    transfer_file: Path,
):
    """
    Validate multiple manifest files for schema errors.
    """

    cred_config = None
    transfer_config = None  # pylint: disable=unused-variable
    manifests = []  # pylint: disable=unused-variable

    if credentials_file is not None:
        cred_config = Credentials.load_file(credentials_file)
    if transfer_file is not None:
        transfer_config = Transfer.load_file(transfer_file)

    manifests = [Manifest.load_file(file, cred_config) for file in input_files]
