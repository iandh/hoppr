"""
Plugin to cascade through various collectors to try to capture an artifact
"""
import logging

from typing import Any, Dict, Optional

from hoppr import __version__
from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process
from hoppr.context import Context
from hoppr.exceptions import HopprPluginError
from hoppr.hoppr_types.component_coverage import ComponentCoverage
from hoppr.result import Result
from hoppr.utils import plugin_instance


class CompositeCollector(HopprPlugin):
    """
    Plugin to cascade through various collectors to try to capture an artifact
    """

    default_component_coverage = ComponentCoverage.EXACTLY_ONCE

    def get_version(self) -> str:
        return __version__

    def __init__(self, context: Context, config: Optional[Dict] = None) -> None:
        super().__init__(context=context, config=config)

        if self.config is None:
            raise HopprPluginError("CompositeCollector requires configuration be defined in the transfer config file")

        if (
            "plugins" not in self.config
            or not isinstance(self.config["plugins"], list)
            or len(self.config["plugins"]) == 0
        ):
            raise HopprPluginError("CompositeCollector requires at least one plugin be configured")

        self.child_plugins: list[HopprPlugin] = []

        for plugin_spec in self.config["plugins"]:
            plugin = plugin_instance(plugin_spec["name"], context, plugin_spec.get("config"))
            self.child_plugins.append(plugin)

    @hoppr_process
    def pre_stage_process(self):
        """
        Run all child plugin pre-stage processes
        """

        for plugin in self.child_plugins:
            result = plugin.pre_stage_process()
            if result.is_fail() or result.is_retry():
                return Result.fail(f"Failure running {plugin.__class__.__name__}")

        return Result.success()

    @hoppr_process
    def process_component(self, comp: Any) -> Result:  # pylint: disable=unused-argument
        """
        Run component through each child plugin component processes
        """

        for plugin in self.child_plugins:
            self._log_and_flush(f"Attempting to process {comp.purl} using {plugin.__class__.__name__}")

            result = plugin.process_component(comp)
            if result.is_success():
                return Result.success(f"Used {plugin.__class__.__name__}")

        return Result.fail(f"Failed to run component {comp.purl} through all child plugins.")

    @hoppr_process
    def post_stage_process(self):
        """
        Run all child plugin post-stage processes
        """

        for plugin in self.child_plugins:
            result = plugin.post_stage_process()
            if result.is_fail() or result.is_retry():
                return Result.fail(f"Failure running {plugin.__class__.__name__}")

        return Result.success()

    def _log_and_flush(self, msg: str, level: int = logging.INFO, indent_level: int = 0):
        self.get_logger().log(level, msg, indent_level)
        self.get_logger().flush()

    def supports_purl_type(self, purl_type: str) -> bool:
        """
        Composite collector purl type support is based on first child collector
        """

        return self.child_plugins[0].supports_purl_type(purl_type)
