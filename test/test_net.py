import json
import unittest

from test.test_utils import MockedResponse
from unittest import mock

import pytest

import hoppr.net
import hoppr.plugin_utils
import hoppr.utils

from hoppr.configs.credentials import Credentials
from hoppr.exceptions import HopprLoadDataError
from hoppr.hoppr_types.cred_object import CredObject

test_input_data = {
    "alpha": [1, 2, 3],
    "beta": ["dog", "cat"],
    "gamma": {"x": 42, "y": "why not", "z": ["mixed", 7, "array"]},
}

class TestNet(unittest.TestCase):
    @mock.patch("requests.get", return_value=MockedResponse(200, content=json.dumps(test_input_data)))
    def test_load_url_good(self, mock_get):
        content = hoppr.net.load_url("url_goes_here")
        assert content == test_input_data

    @mock.patch("requests.get", return_value=MockedResponse(200, content=test_input_data))
    def test_load_url_unsupported_input_type(self, mock_get):
        with pytest.raises(HopprLoadDataError):
            content = hoppr.net.load_url("url_goes_here")

    @mock.patch("requests.get", return_value=MockedResponse(200, content=json.dumps(test_input_data).encode("utf-8")))
    def test_load_url_good_as_bytes(self, mock_get):
        content = hoppr.net.load_url("url_goes_here")
        assert content == test_input_data

    @mock.patch("requests.get", return_value=MockedResponse(200, content=json.dumps(test_input_data)))
    def test_load_url_good_with_credentials(self, mock_get):
        content = hoppr.net.load_url("url_goes_here", CredObject("testUser","testPassword"))
        assert content == test_input_data

    @mock.patch("hoppr.net.load_string", side_effect=HopprLoadDataError("TEST ERROR"))
    @mock.patch("requests.get", return_value=MockedResponse(200, content=json.dumps(test_input_data)))
    def test_load_url_bad_data(self, mock_get, mock_load):
        with pytest.raises(HopprLoadDataError):
            content = hoppr.net.load_url("url_goes_here")

    @mock.patch("builtins.open")
    @mock.patch("requests.get", return_value=MockedResponse(200, content="stuff"))
    def test_download_file(self, mock_get, mock_open):
        resp = hoppr.net.download_file("http://dummy.com", "whereever", CredObject("testUser","testPassword"))
        assert resp.status_code == 200

    @mock.patch("builtins.open")
    @mock.patch("requests.get", return_value=MockedResponse(200, content="stuff"))
    @mock.patch.object(Credentials, "find_credentials", return_value= CredObject("fake_user", "fake_pw"))
    def test_download_file_without_credentials(self, mock_creds, mock_get, mock_open):
        resp = hoppr.net.download_file("http://dummy.com", "whereever")
        assert resp.status_code == 200
