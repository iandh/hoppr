"""
Shareable fixtures for unit tests
"""
import json
import multiprocessing

from pathlib import Path
from subprocess import CompletedProcess
from typing import Callable, Dict, List, Optional

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import CyclonedxSoftwareBillOfMaterialsStandard as Bom_1_4  # type: ignore
from pytest import FixtureRequest

from hoppr.base_plugins.hoppr import HopprPlugin
from hoppr.configs.manifest import Manifest
from hoppr.context import Context
from hoppr.hoppr_types.cred_object import CredObject

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument


@pytest.fixture(autouse=True)
def print_test_name(request: FixtureRequest):
    """
    Print name of test
    """
    print(f"-------- Starting {request.node.name} --------")


@pytest.fixture
def completed_process_fixture(request: FixtureRequest) -> CompletedProcess:
    """
    CompletedProcess fixture for subprocess commands

    Intended for use with `pytest` test modules. For `unittest.TestCase` based
    classes, use the `test.mock_objects.MockSubprocessRun` mock object.
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["args"] = param_dict.get("args", ["mock", "command"])
    param_dict["returncode"] = param_dict.get("returncode", 0)
    param_dict["stdout"] = param_dict.get("stdout", b"Mock stdout")
    param_dict["stderr"] = param_dict.get("stderr", b"Mock stderr")

    return CompletedProcess(**param_dict)


@pytest.fixture
def config_fixture(request: FixtureRequest) -> Dict[str, str]:
    """
    Test plugin config fixture
    """
    param_dict = dict(getattr(request, "param", {}))
    return param_dict


@pytest.fixture
def context_fixture(request: FixtureRequest, manifest_fixture: Manifest, tmp_path: Path) -> Context:
    """
    Test Context fixture
    """
    sbom_path = Path(__file__).parent / "resources" / "bom" / "unit_bom1_mini.json"

    if hasattr(request, "cls") and request.cls is not None:
        sbom_path = getattr(request.cls, "sbom_path", sbom_path)
    else:
        param_dict = dict(getattr(request, "param", {}))
        sbom_path = param_dict.get("sbom_path", sbom_path)

    with sbom_path.open(mode="r", encoding="utf-8") as bom_file:
        bom_dict = json.load(fp=bom_file)

    sbom = Bom_1_4(**bom_dict)

    return Context(
        manifest=manifest_fixture,
        collect_root_dir=str(tmp_path),
        consolidated_sbom=sbom,
        delivered_sbom=sbom,
        retry_wait_seconds=1,
        max_processes=3,
        logfile_lock=multiprocessing.Manager().RLock(),
    )


@pytest.fixture
def cred_object_fixture(request: FixtureRequest):
    """
    Test CredObject fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["username"] = param_dict.get("username", "mock_user")
    param_dict["password"] = param_dict.get("password", "mock_password")

    return CredObject(**param_dict)


@pytest.fixture
def find_credentials_fixture(cred_object_fixture: CredObject) -> Callable[[str], CredObject]:
    """
    Override cred_object_fixture to return Callable
    """

    def _find_credentials(url: str) -> CredObject:
        return cred_object_fixture

    return _find_credentials


@pytest.fixture
def manifest_fixture(request: FixtureRequest) -> Manifest:
    """
    Test Manifest fixture
    """
    manifest_path = Path(__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"

    if hasattr(request, "cls") and request.cls is not None:
        manifest_path = getattr(request.cls, "manifest_path", manifest_path)
    else:
        param_dict = dict(getattr(request, "param", {}))
        manifest_path = param_dict.get("manifest_path", manifest_path)

    return Manifest.load_file(manifest_path)


@pytest.fixture
def plugin_fixture(
    request: FixtureRequest, config_fixture: Dict[str, str], context_fixture: Context
) -> Optional[HopprPlugin]:
    """
    # Test collector plugin fixture

    ## Usage

    ### pytest

    To use this fixture in a pytest module, override this fixture in the test module,
    specifying the type of Hoppr plugin to return in the fixture params as a dict of
    the form `{"plugin_class": <Hoppr plugin class type>}`. For example:

    ```python
    # test_collect_raw.py

    @pytest.fixture(scope="function", params=[dict(plugin_class=CollectRawPlugin)])
    def plugin_fixture(plugin_fixture: CollectRawPlugin, monkeypatch: MonkeyPatch) -> CollectRawPlugin:
        monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
        return plugin_fixture
    ```

    In this overridden fixture, you can monkeypatch attributes that are common to all
    functions in the test module.

    ### unittest

    For unittest.TestCase based classes, declare the following class attributes:

    - `PluginClass = <Hoppr plugin class type>`
    - `plugin_fixture: PluginClass`

    and decorate the test class with `@pytest.mark.usefixtures("plugin_fixture")`. For example:

    ```python
    @pytest.mark.usefixtures("plugin_fixture")
    class TestCollectRawPlugin(unittest.TestCase):
        PluginClass = CollectRawPlugin
        plugin_fixture: PluginClass

        def test_collect_raw(self):
            self.plugin.config = dict(config_option="config value")
            ...
    ```
    """
    # Fixture requested by a class
    if hasattr(request, "cls") and request.cls is not None:
        plugin_class = getattr(request.cls, "PluginClass", None)
        if plugin_class is None:
            raise pytest.UsageError(f"The PluginClass attribute was not found in class {request.cls}")
    # Fixture requested by a function decorated with `@pytest.mark.parametrize`
    else:
        param_dict = dict(getattr(request, "param", {}))
        plugin_class = param_dict.get("plugin_class")
        if plugin_class is None:
            raise pytest.UsageError("The plugin_class fixture param was not specified")

    if not issubclass(plugin_class, HopprPlugin):
        raise pytest.UsageError(f"Class {plugin_class} is not a subclass of HopprPlugin")

    plugin_obj = plugin_class(context=context_fixture, config=config_fixture)

    # Set the `plugin_fixture` class attribute if fixture requested from a unittest-based class
    if hasattr(request, "cls") and request.cls is not None:
        request.cls.plugin_fixture = plugin_obj

    return plugin_obj


@pytest.fixture
def run_command_fixture(
    completed_process_fixture: CompletedProcess,
) -> Callable[[List[str], Optional[List[str]], Optional[str]], CompletedProcess]:
    """
    Override completed_process_fixture to return Callable
    """

    def _run_command(
        command: List[str], password_list: Optional[List[str]] = None, cwd: Optional[str] = None
    ) -> CompletedProcess:

        return completed_process_fixture

    return _run_command
