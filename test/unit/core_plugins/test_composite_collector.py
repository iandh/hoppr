"""
Test module for CompositeCollector class
"""

# pylint: disable=missing-function-docstring

import multiprocessing

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component

from hoppr.configs.manifest import Manifest
from hoppr.context import Context
from hoppr.core_plugins.collect_dnf_plugin import CollectDnfPlugin
from hoppr.core_plugins.collect_nexus_search import CollectNexusSearch
from hoppr.core_plugins.composite_collector import CompositeCollector
from hoppr.exceptions import HopprPluginError
from hoppr.hoppr_types.purl_type import PurlType
from hoppr.result import Result


def _create_test_plugin(config=None):
    test_manifest = Manifest()
    print(f"DEBUG: manifest {test_manifest}")
    test_manifest.consolidated_repositories = {PurlType.RPM: []}
    context = Context(
        manifest=test_manifest,
        collect_root_dir="COLLECTION_DIR",
        consolidated_sbom="BOM",
        delivered_sbom="BOM",
        retry_wait_seconds=1,
        max_processes=3,
        logfile_lock=multiprocessing.Manager().RLock(),
    )
    my_plugin = CompositeCollector(context=context, config=config)
    return my_plugin


_test_config = {
    "plugins": [{"name": "hoppr.core_plugins.collect_dnf_plugin"}, {"name": "hoppr.core_plugins.collect_nexus_search"}]
}


def _create_test_component(name="test_component", type="rpm", version="0.1.2"):  # pylint: disable=redefined-builtin
    purl = f"pkg:{type}/{name}@{version}"
    return Component(name=name, purl=purl, type="file")


def test_get_version():
    my_plugin = _create_test_plugin(config=_test_config)
    assert len(my_plugin.get_version()) > 0


def test_composite_pre_no_config():
    with pytest.raises(HopprPluginError):
        _create_test_plugin()


def test_composite_pre_no_children():
    with pytest.raises(HopprPluginError):
        _create_test_plugin(config={"plugins": []})


def test_composite_pre_success(mocker):
    mock_dnf_pre = mocker.patch.object(CollectDnfPlugin, "pre_stage_process", return_value=Result.success())
    mock_nexus_pre = mocker.patch.object(CollectNexusSearch, "pre_stage_process", return_value=Result.success())

    my_plugin = _create_test_plugin(config=_test_config)

    composite_result = my_plugin.pre_stage_process()
    assert composite_result.is_success(), f"Expected SUCCESS result, got {composite_result}"
    assert mock_dnf_pre.call_count == 1
    assert mock_nexus_pre.call_count == 1


def test_composite_pre_fail(mocker):
    mock_dnf_pre = mocker.patch.object(CollectDnfPlugin, "pre_stage_process", return_value=Result.success())
    mock_nexus_pre = mocker.patch.object(CollectNexusSearch, "pre_stage_process", return_value=Result.fail())

    my_plugin = _create_test_plugin(config=_test_config)

    composite_result = my_plugin.pre_stage_process()
    assert composite_result.is_fail(), f"Expected FAIL result, got {composite_result}"
    assert mock_dnf_pre.call_count == 1
    assert mock_nexus_pre.call_count == 1


def test_composite_comp_success(mocker):
    mock_dnf_comp = mocker.patch.object(CollectDnfPlugin, "process_component", return_value=Result.success())
    mock_nexus_comp = mocker.patch.object(CollectNexusSearch, "process_component", return_value=Result.fail())

    my_plugin = _create_test_plugin(config=_test_config)

    composite_result = my_plugin.process_component(_create_test_component())
    assert composite_result.is_success(), f"Expected SUCCESS result, got {composite_result}"
    assert mock_dnf_comp.call_count == 1
    assert mock_nexus_comp.call_count == 0


def test_composite_comp_fail(mocker):
    mock_dnf_comp = mocker.patch.object(CollectDnfPlugin, "process_component", return_value=Result.fail())
    mock_nexus_comp = mocker.patch.object(CollectNexusSearch, "process_component", return_value=Result.fail())

    my_plugin = _create_test_plugin(config=_test_config)

    composite_result = my_plugin.process_component(_create_test_component())
    assert composite_result.is_fail(), f"Expected FAIL result, got {composite_result}"
    assert mock_dnf_comp.call_count == 1
    assert mock_nexus_comp.call_count == 1


def test_composite_post_success(mocker):
    mock_dnf_post = mocker.patch.object(CollectDnfPlugin, "post_stage_process", return_value=Result.success())
    mock_nexus_post = mocker.patch.object(CollectNexusSearch, "post_stage_process", return_value=Result.success())

    my_plugin = _create_test_plugin(config=_test_config)

    composite_result = my_plugin.post_stage_process()
    assert composite_result.is_success(), f"Expected SUCCESS result, got {composite_result}"
    assert mock_dnf_post.call_count == 1
    assert mock_nexus_post.call_count == 1


def test_composite_post_fail(mocker):
    mock_dnf_post = mocker.patch.object(CollectDnfPlugin, "post_stage_process", return_value=Result.success())
    mock_nexus_post = mocker.patch.object(CollectNexusSearch, "post_stage_process", return_value=Result.fail())

    my_plugin = _create_test_plugin(config=_test_config)

    composite_result = my_plugin.post_stage_process()
    assert composite_result.is_fail(), f"Expected FAIL result, got {composite_result}"
    assert mock_dnf_post.call_count == 1
    assert mock_nexus_post.call_count == 1
