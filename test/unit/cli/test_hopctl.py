"""
Test module for hopctl.py
"""
import re

from pytest import CaptureFixture

from hoppr.cli.hopctl import version as hopctl_version


def test_version(capsys: CaptureFixture):
    """
    Test `hopctl version` output
    """
    hopctl_version()
    captured = capsys.readouterr()

    matched = re.match(
        r"\n".join([".*", "Hoppr Framework Version : .*", "Python Version          : .*"]),
        captured.out,
        flags=re.DOTALL,
    )

    assert matched is not None
