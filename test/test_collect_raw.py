import multiprocessing

from pathlib import Path
from test.mock_objects import MockHttpResponse
from unittest import TestCase
from unittest.mock import patch

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component

from hoppr.configs.credentials import Credentials
from hoppr.context import Context
from hoppr.core_plugins.collect_raw_plugin import CollectRawPlugin
from hoppr.hoppr_types.cred_object import CredObject


class TestCollectorRaw(TestCase):
    def _create_test_plugin(self):
        context = Context(
            manifest="MANIFEST",
            collect_root_dir="COLLECTION_DIR",
            consolidated_sbom="BOM",
            delivered_sbom="BOM",
            retry_wait_seconds=1,
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )
        my_plugin = CollectRawPlugin(context=context, config="CONFIG")
        return my_plugin

    @patch.object(Credentials, "find_credentials", return_value= CredObject("fake_user", "fake_pw"))
    @patch.object(CollectRawPlugin, "_get_repos", return_value=["https://somewhere.com"])
    @patch("hoppr.core_plugins.collect_raw_plugin.download_file", return_value=MockHttpResponse(200, content="mocked content"))
    def test_collector_raw_url(self, mock_download, mock_get_repos, mock_find_creds):
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        self.assertTrue(collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}")

    @patch.object(Credentials, "find_credentials", return_value= CredObject("fake_user", "fake_pw"))
    @patch.object(CollectRawPlugin, "_get_repos", return_value=["https://somewhere.com"])
    @patch("hoppr.core_plugins.collect_raw_plugin.download_file", return_value=MockHttpResponse(404, reason="mocked download fail message"))
    def test_collector_raw_download_file_fail(self, mock_download, mock_get_repos, mock_find_creds):
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_fail(), f"Expected FAIL result, got {collect_result}")
        self.assertEqual(collect_result.message, f"HTTP Status Code: {mock_download.return_value.status_code}; {mock_download.return_value.reason}")

    @patch.object(CollectRawPlugin, "_get_repos", return_value=["file://somewhere.com"])
    @patch("shutil.copy")
    @patch.object(Path, "is_file", return_value=True)
    def test_collector_raw_file(self, mock_path_isfile, mock_sh_copy, mock_get_repos):
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        self.assertTrue(collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}")

    @patch.object(CollectRawPlugin, "_get_repos", return_value=["file://somewhere.com"])
    @patch("shutil.copy")
    @patch.object(Path, "is_file", return_value=False)
    def test_collector_raw_file_not_found(self, mock_path_isfile, mock_sh_copy, mock_get_repos):
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        self.assertTrue(collect_result.is_fail(), f"Expected FAIL result, got {collect_result}")

    @patch.object(CollectRawPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_raw_url_mismatch(self, mock_get_repos):
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3?repository_url=my.repo", type="file")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_fail(), f"Expected FAIL result, got {collect_result}")
        self.assertEqual(collect_result.message, "Purl-specified repository url (my.repo) does not match current repo (https://somewhere.com).")

    def test_get_version(self):
        my_plugin = self._create_test_plugin()
        self.assertGreater(len(my_plugin.get_version()), 0)
