ARG BASE_IMAGE=docker.io/library/ubuntu
ARG BASE_TAG=kinetic-20221101

# ----------------------------------------
# Hoppr install stage
# ----------------------------------------
FROM $BASE_IMAGE:$BASE_TAG AS builder
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG APT_PKGS="curl dnf git maven python3 python3-apt python3-pip skopeo"

COPY dist/hoppr-*-py3-none-any.whl /tmp

# hadolint ignore=DL3008
RUN apt-get update \
  && apt-get install --yes --no-install-recommends ${APT_PKGS} \
  && apt-get clean \
  && rm -r /var/lib/apt/lists/* \
  && mkdir --parents /etc/yum.repos.d \
  && export PIP_TRUSTED_HOST="pypi.org pypi.python.org files.pythonhosted.org" \
  && python3 -m pip install --no-cache-dir /tmp/hoppr-*-py3-none-any.whl \
  && rm /tmp/hoppr-*-py3-none-any.whl \
  && curl --fail --silent --show-error --location --url https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash \
  && apt-get autoremove --yes curl

# ----------------------------------------
# Final stage
# ----------------------------------------
FROM $BASE_IMAGE:$BASE_TAG

# Flatten build layers into single layer
COPY --from=builder / /

VOLUME /hoppr
WORKDIR /hoppr
ENTRYPOINT ["/usr/local/bin/hopctl"]
