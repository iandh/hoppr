![Hoppr repository banner](media/hoppr-repo-banner.png)

---
## What is Hoppr?

Hoppr is a Python plugin-based framework for collecting, processing, and bundling your software supply chain.
Feed Hoppr your [CycloneDX spec SBOMs](https://cyclonedx.org/specification/overview/) (Software Bill of Materials) and receive enhanced
SBOMs and component bundles in return!  Those are just the basics, learn more at:

**The Documentation**: <https://hoppr.dev>

**Source Code**: <https://gitlab.com/hoppr/hoppr>

---

## Getting Started

For a full Hoppr startup, we recommend the [`Your First Bundle` tutorial](https://hoppr.dev/docs/get-started/your-first-bundle)

Install [Hoppr from PyPI](https://pypi.org/project/hoppr/)
```
pip install hoppr
```

## Join Us!

We're completely open source, [MIT licensed](LICENSE), and community friendly. Built with a plugin architecture, Hoppr enables users to extend its SBOM-processing capabilities through their own plugins and algorithms.  
[Learn more](https://hoppr.dev/docs/development/contributing) on how to contribute, it's easy and welcoming!